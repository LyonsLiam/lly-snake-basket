from Person import Person


def main():
    print("Enter your name")
    name = input()
    print("Enter your age")
    age = input()
    p = Person(name, age)
    p.print_information()


if __name__ == '__main__':
    main()
